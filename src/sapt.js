import './sapt.scss';

// Handle pricing table height equalization
if ( $( '.sapt-pricing-table' ).length === 3 ) {
	var height = 0;
	$( '.sapt-pricing-table' ).each( function() {
		if ( $( this ).outerHeight() > height && ! $( this ).hasClass( 'most-popular' ) ) {
			height = $( this ).outerHeight();
		}
	} );

	$( '.sapt-pricing-table' ).each( function() {
		if ( $( this ).outerHeight() === height ) {
			$ ( this ).addClass( 'tallest' );
		} else if ( ! $( this ).hasClass( 'most-popular' ) ) {
			$( this ).addClass( 'shortest' );
		}
	} );
}

function sizePriceTables() {

	if ( $( window ).innerWidth() < 768 ) {
		$( '.sapt-pricing-table.shortest' ).css( 'height', 'auto' );
	} else {
		$( '.sapt-pricing-table.shortest' ).css( 'height', $( '.sapt-pricing-table.tallest' ).outerHeight() + 'px' );
	}

}

sizePriceTables();

$( window ).resize( sizePriceTables );

// Handle hover info button
$( '.sapt-info-icon' ).hover( function() {
	// If mobile, don't handle overflow issues
	if ( $( window ).innerWidth() < 768 ) {
		$( this ).next().show();
		return;
	}
	$( '.sapt-info.active' ).removeClass( 'active' );
	breakOverflow( $( this ).next() );
}, function() {
	// If mobile, don't handle overflow issues
	if ( $( window ).innerWidth() < 768 ) {
		$( this ).next().hide();
		return;
	}
	var active = $( '.sapt-info.active' );
	active.css( 'display', 'none' );
	$( this ).after( active );
} );

// Function to break overflow by appending to the body
function breakOverflow( element ) {
	console.log( element );
	var top = element.prev().offset().top - 50;
	var left = element.prev().offset().left - 20;
	element.addClass( 'active' );
	element.appendTo( $( 'body' ) );
	element.css( {
		bottom: 'auto',
		display: 'block',
		left: left + 'px',
		position: 'absolute',
		right: 'auto',
		top: top + 'px',
		'z-index': 10000
	} );
}