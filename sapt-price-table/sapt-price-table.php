<?php

class SaptPriceTableClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Super Awesome Pricing Table', 'sapt' ),
            'description'     => __( 'Display a pricing table', 'sapt' ),
            'category'        => __( 'Advanced', 'sapt-text' ),
            'dir'             => SAPT_MODULES_DIR . 'sapt-price-table/',
            'url'             => SAPT_MODULES_URL . 'sapt-price-table/',
            'icon'            => 'editor-table.svg',
        ));
    }
}

FLBuilder::register_module( 'SaptPriceTableClass', array(
    'sapt-laptop-tab-1'  => array(
        'title'     => __( 'Options', 'sapt-text' ),
        'sections'  => array(
            'sapt-section-1'    => array(
                'title'     => __( 'Section 1', 'sapt-text' ),
                'fields'    => array(
                    'sapt_price_popular'  => array(
                        'type'          => 'text',
                        'label'         => __( '(Optional) If most popular, enter text for top', 'sapt-text' ),
                        'placeholder' => 'Most Popular',
                    ),
                    'sapt_price_title'  => array(
                        'type'          => 'text',
                        'label'         => __( 'The top label for the pricing table', 'sapt-text' ),
                        'placeholder' => 'Beginner',
                    ),
                    'sapt_price_currency'    => array(
                        'type'  => 'text',
                        'label' => __( 'Currency', 'sapt-text' ),
                        'placeholder' => '$',
                    ),
                    'sapt_price_price'    => array(
                        'type'  => 'text',
                        'label' => __( 'The price', 'sapt-text' ),
                        'placeholder' => '99',
                    ),
                    'sapt_price_length'    => array(
                        'type'  => 'text',
                        'label' => __( 'Length of price term (YR, MO, etc.)', 'sapt-text' ),
                        'placeholder' => 'YR',
                    ),
                    'sapt_price_quote'    => array(
                        'type'  => 'text',
                        'label' => __( 'Quote placed below price', 'sapt-text' ),
                        'placeholder' => 'An entry tool for building a membership site when you\'re just getting started.',
                    ),
                    'sapt_price_action_text'    => array(
                        'type'  => 'text',
                        'label' => __( 'Text of the action button', 'sapt-text' ),
                        'placeholder' => 'Get Started',
                    ),
                    'sapt_price_action_link'    => array(
                        'type'  => 'link',
                        'label' => __( 'Link of the action button', 'sapt-text' ),
                    ),
                    'sapt_price_additional'    => array(
                        'type'  => 'text',
                        'label' => __( 'Optional text to be displayed on desktop for copy like "Includes Beginner features, and..."', 'sapt-text' ),
                    ),
                    'sapt_price_features'     => array(
                        'type' => 'textarea',
                        'label' => 'Features List',
                        'multiple' => true,
                    ),
                    'sapt_price_features_button'    => array(
                        'type'  => 'text',
                        'label' => __( 'Features link text', 'sapt-text' ),
                        'placeholder' => 'See All Features',
                    ),
                    'sapt_price_features_link'    => array(
                        'type'  => 'link',
                        'label' => __( 'Link to features page', 'sapt-text' ),
                    ),
                )
            )
        )
    )
) );