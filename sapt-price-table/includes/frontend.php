<div class="sapt-pricing-table<?php echo $settings->sapt_price_popular ? ' most-popular' : '' ; ?>">
	<?php
		if ( $settings->sapt_price_popular ) { ?>
			<h6 class="popular-heading"><?php echo $settings->sapt_price_popular; ?></h6>
		<?php }
	?>

	<h3><?php echo $settings->sapt_price_title; ?></h3>
	<div class="price">
		<h4><span class="currency"><?php echo $settings->sapt_price_currency; ?></span><?php echo $settings->sapt_price_price; ?><span class="length"> / <?php echo $settings->sapt_price_length; ?></span></h4>
	</div>
	<div class="quote">
		<?php echo $settings->sapt_price_quote; ?> 
	</div>
	<div class="action">
		<a href="<?php echo $settings->sapt_price_action_link; ?>" class="button"><?php echo $settings->sapt_price_action_text; ?></a>
	</div>
	<div class="feature-list">
		<?php if ( $settings->sapt_price_additional ) {
			echo '<p class="additional"><strong>' . $settings->sapt_price_additional . '</strong></p>';
		} ?>
		<ul>
			<?php foreach( $settings->sapt_price_features as $setting ) { ?>
				<li><?php echo $setting; ?></li>
			<?php } ?>
		</ul>
	</div>
	<a href="<?php echo $settings->sapt_price_features_link; ?>" class="feature-link"><?php echo $settings->sapt_price_features_button; ?></a>
</div>