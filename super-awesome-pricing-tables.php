<?php
/**
 * Plugin Name: Super Awesome Pricing Tables
 * Plugin URI: https://pixeljar.com
 * Description: Super Awesome Pricing Tables for Beaver Builder
 * Version: 1.0.0
 * Author: Pixel Jar
 * Author URI: https://pixeljar.com
 * Text Domain: sapt-text
 */

define( 'SAPT_MODULES_DIR', plugin_dir_path( __FILE__ ) );
define( 'SAPT_MODULES_URL', plugins_url( '/', __FILE__ ) );

function sapt_load_modules() {
	if( class_exists( 'FLBuilder' ) ) {
		require_once 'sapt-price-table/sapt-price-table.php';
	}
}

add_action( 'init', 'sapt_load_modules' );
