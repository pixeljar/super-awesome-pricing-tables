# Super Awesome Pricing Tables for Beaver Builder

Pricing tables should be inside of a 3-column row with this class:
```sapt-pricing-table-row```

This row should **not** be nested.

By default, price tables that have text in the "Most popular" field will have a different background color.

### Build

Do not edit frontend.{js, css}. Source files exist in src folder.

Edit colors at the top of sapt.scss.

You may also want to edit the fills in the svg files.

Run ```npm install``` to set up the project
Run ```npm run build``` to build js and sass

### Info box in feature list

```<span class="sapt-info-icon"></span><span class="sapt-info">TEXT TO BE SHOWN ON HOVER GOES HERE</span>```