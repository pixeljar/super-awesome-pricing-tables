const path = require( 'path' );
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );
const webpack = require( 'webpack' );

module.exports = {
	entry: {
		'sapt-price-table/js/frontend': './src/sapt',
	},
	output: {
		path: path.resolve( __dirname, './' ),
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.svg$/,
				use: 'url-loader',
			},
			{
				test: /\.s?(c|a)ss/,
				use: ExtractTextPlugin.extract({
					use: [
						{
							loader: 'css-loader',
							options: {
								minimize: true,
							},
						},
						{
							loader: 'sass-loader',
						},
					],
					publicPath: './sapt-price-table/css/',
				})
			},
		],
	},
	plugins: [
		new webpack.optimize.UglifyJsPlugin(),
		new ExtractTextPlugin( 'sapt-price-table/css/frontend.css' ),
	],
}
